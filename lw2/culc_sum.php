<?php
    $num1 = '';
    if (isset($_GET['arg1']))
    {
        $num1 = $_GET['arg1'];
    }
    $num2 = '';
    if (isset($_GET['arg2']))
    {
        $num2 = $_GET['arg2'];
    }
    if ( is_numeric($num1) & is_numeric($num2) )
    {
        echo (intval($num1) + intval($num2));
    }
    else
    {
		echo 'Invalid arguments';
        header('Status: 400');
    }
