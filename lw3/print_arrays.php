<?php
	$prime_numbers = [2,3,5,7,11,13,17,19,23,29];
	$function_definition = [
		'count'       => 'Count all elements in an array, or something in an object',
		'is_array'    => 'Finds whether a variable is an array',
		'array_marge' => 'Merge one or more arrays',
		'empty'       => 'Determine whether a variable is empty',
		'print_r'     => 'Prints human-readable information about a variable'
	];
	$e_4x4_matirx = [
		[1, 0, 0, 0],
		[0, 1, 0, 0],
		[0, 0, 1, 0],
		[0, 0, 0, 1]
	];
	
	function print_var_info_in_p ($var)
	{
		echo '<pre>', print_r($var) , '</pre>';
	}
	
	print_var_info_in_p($prime_numbers);
	print_var_info_in_p($function_definition);
	print_var_info_in_p($e_4x4_matirx);
	
