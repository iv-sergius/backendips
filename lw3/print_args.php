<?php
    if ($argc == 1)
    {
        echo 'No parameters were specified!', PHP_EOL;
    }
    else
    {
        $num = $argc - 1;
        echo 'Number of arguments: ',$num , PHP_EOL;
        echo 'Arguments:';
        for ($i = 1; $i < $argc; $i++)
        {
            echo $argv[$i], ' ';
        }
        echo PHP_EOL;
    }
    
